// Fill out your copyright notice in the Description page of Project Settings.

#include "MyCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Camera1Arm = CreateDefaultSubobject<USpringArmComponent>("Cam arm 1");
	Camera1Arm->SetupAttachment(RootComponent);
	Camera2Arm = CreateDefaultSubobject<USpringArmComponent>("Cam arm 2");
	Camera2Arm->SetupAttachment(RootComponent);

	Camera1= CreateDefaultSubobject<UCameraComponent>("Cam 1");
	Camera1->SetupAttachment(Camera1Arm);
	Camera2 = CreateDefaultSubobject<UCameraComponent>("Cam 2");
	Camera2->SetupAttachment(Camera2Arm);

	CamState = true;//TRUE Cam 1, FALSE Cam2
	CurrentCamComp = Camera1;
	//bPlatformerDirection = true; //TRUE right, FALSE left

}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("ChangeCamera",IE_Pressed, 
		this, 
		&AMyCharacter::ChangeCamera);
	PlayerInputComponent->BindAction("Jump", IE_Pressed,
		this,
		&ACharacter::Jump);


	PlayerInputComponent->BindAxis("MoveRight",
		this,
		&AMyCharacter::MoveRight);

}

void AMyCharacter::CalcCamera(float DeltaSeconds, struct FMinimalViewInfo& OutResult) {

	CurrentCamComp->GetCameraView(DeltaSeconds, OutResult);
}

void AMyCharacter::ChangeCamera() {

	CamState = !CamState;

	UCharacterMovementComponent *CharMov = GetCharacterMovement();

	/*if (CharMov->IsFalling()) {
		return;
	}*/

	if (CamState) { // Platformer camera
		bUseControllerRotationYaw = false;
		CharMov->bOrientRotationToMovement = true;
		CurrentCamComp = Camera1;
	}else { //Cenital
		bUseControllerRotationYaw = true;
		CharMov->bOrientRotationToMovement = false;
		CurrentCamComp = Camera2;
	}
}

void AMyCharacter::MoveRight(float val) {


	//AController * MyController = GetController();
	//MyController->ControlRotation
	if (CamState) { //Platformer Camera
		AddMovementInput(FVector::ForwardVector, val);
	}
	else { //Cenital
		AddControllerYawInput(val);
	}

}

