// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "MyCharacter.generated.h"

UCLASS()
class CAMERATEST_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent *Camera1Arm;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent *Camera2Arm;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UCameraComponent *Camera1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UCameraComponent *Camera2;

	UPROPERTY()
		bool CamState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
		TArray<AActor*> ActorList;


private:

	UPROPERTY()
		UCameraComponent *CurrentCamComp;



public:	

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CalcCamera(float DeltaSeconds, struct FMinimalViewInfo& OutResult) override;

	UFUNCTION()
		void ChangeCamera();

	UFUNCTION()
		void MoveRight(float val);
	
};
